import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComposerPage } from './composer.page';

describe('ComposerPage', () => {
  let component: ComposerPage;
  let fixture: ComponentFixture<ComposerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComposerPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComposerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
